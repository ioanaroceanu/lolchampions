package com.training.lab.controller;

import com.training.lab.entities.Champion;
import com.training.lab.service.ChampionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class ChampionController {

    @Autowired
    private ChampionService championService;

    @GetMapping
    public List<Champion> findAll() {
        return championService.findAll();
    }

    @PostMapping
    public ResponseEntity<Champion> save(@RequestBody Champion champion){
        return new ResponseEntity<Champion>(championService.save(champion), HttpStatus.CREATED);
    }
}
