package com.training.lab.repository;

import com.training.lab.entities.Champion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface ChampionRepository extends JpaRepository<Champion, Long> {
}
