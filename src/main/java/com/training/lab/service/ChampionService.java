package com.training.lab.service;

import com.training.lab.entities.Champion;
import com.training.lab.repository.ChampionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChampionService {

    @Autowired
    ChampionRepository championRepository;

    public List<Champion> findAll() {
        return championRepository.findAll();
    }

    public Champion save(Champion champion){
        return championRepository.save(champion);
    }

}
